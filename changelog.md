# Change log

### 1.1
* Add symbols '<' and '>' for interpretation of symbolic functions.

## 1.0
* Initial release.