﻿using CE.Math.Structs;
using NUnit.Framework;

namespace Pruebas
{
	[TestFixture]
	public class Rationals
	{
		[Test]
		public void NaNIsNaN ()
		{
			Assert.True (Rational.IsNaN (Rational.NaN));
		}

		[Test]
		public void InftyNoNaN ()
		{
			Assert.False (Rational.IsNaN (Rational.PositiveInfinity));
			Assert.False (Rational.IsNaN (Rational.NegativeInfinity));
		}

		[Test]
		public void ApproxHalf ()
		{
			var rt = Rational.FromDouble (0.500000001, 3);
			Assert.AreEqual (0.5, rt, 0.0000001);
		}

		[Test]
		public void NaNPows ()
		{
			Assert.True (Rational.Pow (Rational.NaN, 0) == Rational.NaN);
			Assert.True (Rational.Pow (Rational.NaN, 1) == Rational.NaN);
		}

		[Test]
		public void PosInftyPow ()
		{
			Assert.True (Rational.Pow (Rational.PositiveInfinity, 0) == Rational.NaN);
			Assert.True (Rational.Pow (Rational.PositiveInfinity, 1) == Rational.PositiveInfinity);
		}

		[Test]
		public void NegInftyPow ()
		{
			Assert.True (Rational.Pow (Rational.NegativeInfinity, 0) == Rational.NaN);
			Assert.True (Rational.Pow (Rational.NegativeInfinity, 1) == Rational.NegativeInfinity);
		}

		[Test]
		public void RationalDenAlwaysPositive ([Random (-100, 100, 10)] int num, [Random (-100, 100, 10)] int den)
		{
			Assume.That (den != 0);
			var r = Rational.Create (num, den);
			Assert.Positive (r.Denom);
		}
	}
}
