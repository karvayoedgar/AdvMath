﻿using CE.Math.Structs;
using NUnit.Framework;

namespace Pruebas
{
	[TestFixture]
	public class Test
	{
		[Test]
		public void TestMCD()
		{
			Assert.AreEqual(1, Numbers.LCD(3, 2));
			Assert.AreEqual(2, Numbers.LCD(4, 2));
			Assert.AreEqual(2, Numbers.LCD(2, 4));
			Assert.AreEqual(1, Numbers.LCD(2, 3));

			Assert.AreEqual(2, Numbers.LCD(6, 10));
			Assert.AreEqual(2, Numbers.LCD(10, 6));

			Assert.AreEqual(8, Numbers.LCD(8, 8));
			Assert.AreEqual(7, Numbers.LCD(7, 7));
		}

		[Test]
		public void TestFactor()
		{
			Assert.AreEqual(1, Numbers.PrimeDecomposition(41).Count);
			Assert.AreEqual(2, Numbers.PrimeDecomposition(77).Count);
			Assert.AreEqual(1, Numbers.PrimeDecomposition(65536).Count);
			Assert.AreEqual(5, Numbers.PrimeDecomposition(2 * 3 * 5 * 7 * 11).Count);
			Assert.AreEqual(7, Numbers.PrimeDecomposition(128)[2]);
			Assert.AreEqual(1, Numbers.PrimeDecomposition(128 * 3)[3]);
		}
	}
}