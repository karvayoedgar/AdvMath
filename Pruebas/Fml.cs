﻿using CE.Math.Symbolic;
using NUnit.Framework;

namespace Pruebas
{
	[TestFixture(Category = "Function parsing")]
	public class Fml
	{
		[Test, Combinatorial]
		public void EvalUnequality([Random(0, 100, 3)]int left, [Random(0, 100, 3)]int right)
		{
			var expected = left < right;
			var r = new SymbolicFunction($"{left} < {right}");
			var r2 = new SymbolicFunction($"{left} > {right}");
			Assert.AreEqual(left < right ? 1 : 0, r.Evaluate());
			Assert.AreEqual(left > right ? 1 : 0, r2.Evaluate());
			System.Console.WriteLine($"OK: {left} < {right} : {r.Evaluate()}");
			Assert.Pass();
		}

		[Test]
		public void EvalNumber([Random(-1000, 1000, 3)] int n)
		{
			var r = new SymbolicFunction($"{n}").Evaluate();
			Assert.That(r, Is.EqualTo(n));
		}

		[Test]
		public void UnaryMinus([Random(0, 1000, 2)] int n)
		{

			var r = SymbolicFunction.Evaluate($"-{n}");
			Assert.That(r, Is.EqualTo(-n));
		}

		[Test]
		public void EvalSum([Random(-1000, 1000, 3)] int n1, [Random(-1000, 1000, 3)] int n2)
		{
			var r = SymbolicFunction.Evaluate($"{n1} + {n2}");
			Assert.That(r, Is.EqualTo(n1 + n2));
		}

		[Test]
		public void EvalProd([Random(-1000, 1000, 3)] int n1, [Random(-1000, 1000, 3)] int n2)
		{
			var r = SymbolicFunction.Evaluate($"{n1} * {n2}");
			Assert.That(r, Is.EqualTo(n1 * n2));
		}

		[Test]
		public void Minus([Random(-1000, 1000, 3)] int n1, [Random(-1000, 1000, 3)] int n2)
		{
			var r = SymbolicFunction.Evaluate($"{n1} - {n2}");
			Assert.That(r, Is.EqualTo(n1 - n2));
		}

		[Test]
		public void Div([Random(-1000, 1000, 3)] int n1, [Random(-1000, 1000, 3)] int n2)
		{
			Assume.That(n1 * n2 != 0);
			var r = SymbolicFunction.Evaluate($"{n1} / {n2}");
			Assert.AreEqual(n2, r, 0.000001, $"Div: n1={n1}, n2={n2}, r={r}");
		}

		[Test]
		public void GoodOrder(
			[Random(-1000, 1000, 2)] int n1,
			[Random(-1000, 1000, 2)] int n2,
			[Random(-1000, 1000, 2)] int n3)
		{
			var r = SymbolicFunction.Evaluate($"{n1}+{n2}*{n3}");
			Assert.That(r, Is.EqualTo(n1 + n2 * n3));
		}

		[Test]
		public void EvalVar()
		{
			var context = new FunctionEvaluationContext();
			context.Vars["global.var"] = 3;
			var r = SymbolicFunction.Evaluate("global.var", context);
			Assert.That(r, Is.EqualTo(3));
		}

		[Test]
		public void EvalParamlessFunc()
		{
			var context = new FunctionEvaluationContext();
			context.Funcs["global.pi"] = new SymbolicFunc<float>((arg1, arg2) => arg2[0]);
			var r = SymbolicFunction.Evaluate("global.pi(3, 4)", context);
			Assert.That(r, Is.EqualTo(3));
		}

		[Test]
		public void EvalParamFunc()
		{
			var context = new FunctionEvaluationContext();
			context.Vars["global.in"] = 3;
			context.Funcs["global.prod.in"] = new SymbolicFunc<float>
				((arg1, arg2) => arg1.GetVar("global.in") * arg2[0]);
			var r = SymbolicFunction.Evaluate("global.prod.in(2)", context);
			Assert.That(r, Is.EqualTo(6));
		}
	}
}