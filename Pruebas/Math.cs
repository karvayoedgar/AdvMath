﻿using System;
using CE.Math;
using NUnit.Framework;

namespace Pruebas
{
	[TestFixture]
	public class Math
	{
		public void IntPow(
			[Random(0, 10, 3)] long baseValue,
			[Random(0, 10, 3)] long powerValue)
		{
			// var baseValue = r.Next(0, 10);
			// var powerValue = r.Next(0, 10);
			Assume.That(baseValue != 0 || powerValue != 0);
			var expected = (long)System.Math.Pow(baseValue, powerValue);
			var computed = MathBasic.Pow(baseValue, powerValue);
			var relErr = System.Math.Abs((expected - computed) / expected);
			Assert.AreEqual(0, relErr, 0.0001);
		}

		[Test]
		public void FloatPow(
			[Random(0f, 10f, 3)] double baseValue,
			[Random(0, 10, 3)] int powerValue)
		{
			Assume.That(baseValue != 0 || powerValue != 0);
			var expected = (float)System.Math.Pow((float)baseValue, powerValue);
			var computed = MathBasic.Pow(baseValue, powerValue);
			var relErr = System.Math.Abs((expected - computed) / expected);
			Assert.AreEqual(0, relErr, 0.0001);
		}

		[Test]
		public void DoublePow(
			[Random(0d, 10d, 3)] double baseValue,
			[Range(0, 10)] int powerValue)
		{
			Assume.That(baseValue != 0 || powerValue != 0);
			var expected = System.Math.Pow(baseValue, powerValue);
			var computed = MathBasic.Pow(baseValue, powerValue);
			var relErr = System.Math.Abs((expected - computed) / expected);
			Assert.AreEqual(0, relErr, 0.0001);
		}

		[Test]
		public void ZeroRaiseToZero()
		{
			Assert.Throws<InvalidOperationException>(delegate { MathBasic.Pow(0, 0); });
		}

		[Test]
		public void NegativePower()
		{
			Assert.Throws<InvalidOperationException>(delegate
			{
				MathBasic.Pow(2, -1);
			});
		}

		[Test]
		public void Log(
			[Random(2, 10, 3)] int baseValue,
			[Random(0, 1000, 40)] int argValue)
		{
			var expected = (int)System.Math.Ceiling(System.Math.Log(argValue, baseValue));
			Assert.AreEqual(expected, MathBasic.Log(baseValue, argValue));
		}
	}
}
