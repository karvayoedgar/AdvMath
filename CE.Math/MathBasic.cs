﻿using System;

namespace CE.Math
{
	/// <summary>
	/// Provides static math functions, as some extension of <see cref="System.Math"/>
	/// </summary>
	public static class MathBasic
	{
		/// Raises the specified intenger number by a power.
		/// <param name="base">Base. number</param>
		/// <param name="power">Power to raise</param>
		public static long Pow(long @base, long power)
		{
			if (power < 0) throw new InvalidOperationException("Cannot power to a negative exponent");
			if (power == 0) return @base == 0 ? throw new InvalidOperationException("0^0 is not defined.") : 1;
			if (@base == 0) return 0;

			return UncheckedPow(@base, power);
		}

		/// Raises the specified intenger number by a power.
		/// <param name="base">Base. number</param>
		/// <param name="power">Power to raise</param>
		public static float Pow(float @base, long power)
		{
			if (power < 0) throw new InvalidOperationException("Cannot power to a negative exponent");
			if (power == 0) return @base == 0 ? throw new InvalidOperationException("0^0 is not defined.") : 1;
			if (@base == 0) return 0;

			return UncheckedPow(@base, power);
		}

		/// Raises the specified intenger number by a power.
		/// <param name="base">Base. number</param>
		/// <param name="power">Power to raise</param>
		public static double Pow(double @base, long power)
		{
			if (power < 0) throw new InvalidOperationException("Cannot power to a negative exponent");
			if (power == 0) return @base == 0 ? throw new InvalidOperationException("0^0 is not defined.") : 1;
			if (@base == 0) return 0;

			return UncheckedPow(@base, power);
		}

		/// Gets the smallest number such that its power if not lower than the specified arg.
		public static long Log(double @base, double arg)
		{
			if (arg < 1) return Log(@base, 1d / arg);
			var ret = 0;
			var accum = 1d;
			while (accum < arg)
			{
				ret++;
				accum *= @base;
			}
			return ret;
		}

		static long UncheckedPow(long @base, long power)
		{
			if (power == 0) return 1;
			var divExp = UncheckedPow(@base, power / 2);
			return power % 2 == 0 ? divExp * divExp : divExp * divExp * @base;
		}
		static float UncheckedPow(float @base, long power)
		{
			if (power == 0) return 1;
			var divExp = UncheckedPow(@base, power / 2);
			return power % 2 == 0 ? divExp * divExp : divExp * divExp * @base;
		}
		static double UncheckedPow(double @base, long power)
		{
			if (power == 0) return 1;
			var divExp = UncheckedPow(@base, power / 2);
			return power % 2 == 0 ? divExp * divExp : divExp * divExp * @base;
		}
	}
}