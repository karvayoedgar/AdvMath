﻿using System.Collections.Generic;

namespace CE.Math.Symbolic
{
	/// Common implementation for a generic collection of objects for a context of a function evaluation.
	public abstract class ContextObjectCollection<T>
	{
		Dictionary<string, T> _objs { get; }

		/// Gets or sets the value of a variable
		public T this[string name]
		{
			get
			{
				if (_objs.TryGetValue(name, out var v))
					return v;
				throw new IdentifierException(name, IdentifierException.IdentifierType.Variable);
			}
			set
			{
				_objs[name] = value;
			}

		}

		/// Initializes a new instance of the <see cref="ContextObjectCollection{T}"/> class.
		protected ContextObjectCollection()
		{ _objs = new Dictionary<string, T>(); }
	}
}