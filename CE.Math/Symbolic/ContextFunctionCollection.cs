namespace CE.Math.Symbolic
{
	/// A collection and manager for functions of context for <see cref="SymbolicFunction"/>
	public class ContextFunctionCollection : ContextObjectCollection<ISymbolicFunction<float>> { }
}