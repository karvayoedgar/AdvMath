using System;
using System.Collections.Generic;
using System.Linq;

namespace CE.Math.Symbolic
{
	/// Represents a symbolic function of <c>float</c>.
	public class SymbolicFunction : ISymbolicFunction<float>
	{
		/// Gets the <c>string</c> representation.
		public string StrFormula { get; }

		/// Gets the <c>string</c> representation.
		public override string ToString() => StrFormula;

		/// Creates a new instance.
		/// <param name="s">The representation of a formula.</param>
		public SymbolicFunction(string s)
		{ StrFormula = s.Trim(); }

		/// Evaluate this.
		public float Evaluate() => Evaluate(new FunctionEvaluationContext());

		/// Evaluate this.
		public float Evaluate(IEvalContext<float> context)
		{
			//Orden de eval: Suma, prod, expo, Funciones pred, vars, consts.
			//Suma:
			//Paréntesis:
			float ret = 0;
			string str = StrFormula;

			//Es atómico
			//Paréntesis
			if ((str[0] == Convert.ToChar("(")) && (str.Last() == Convert.ToChar(")")))
			{
				str = str.Remove(0, 1);
				str = str.Remove(str.Length - 1);

				ret += new SymbolicFunction(str).Evaluate(context);
			}
			//número
			if (EsNúmero(str))
			{
				return Convert.ToSingle(str);
			}

			int[] I = BuscarExterior(Convert.ToChar("?"), str);
			if (I.Any())
			{
				// Es condicional :3
				string[] Ls = RompeEnÍndices(str, I);

#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
				return new SymbolicFunction(Ls[0]).Evaluate(context) != 0 ?
									 new SymbolicFunction(Ls[1]).Evaluate(context) :
new SymbolicFunction(Ls[2]).Evaluate(context);
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
			}

			I = BuscarExterior('+', str);
			if (I.Any())
			{
				// Es suma
				ret = 0;
				string[] Ls = RompeEnÍndices(str, I);
				foreach (string s in Ls)
					ret += new SymbolicFunction(s).Evaluate(context);
				return ret;
			}

			I = BuscarExterior('-', str);
			if (I.Any() && I[0] > 0)
			{
				if (I.Length > 1)
					throw new Exception("Invalid format '-' in " + str);
				// Es resta :3
				string[] Ls = RompeEnÍndices(str, I);
				return new SymbolicFunction(Ls[0]).Evaluate(context) - new SymbolicFunction(Ls[1]).Evaluate(context);
			}

			I = BuscarExterior('*', str);
			if (I.Any())
			{
				// Es producto
				ret = 1;
				string[] Ls = RompeEnÍndices(str, I);
				foreach (string s in Ls)
					ret *= new SymbolicFunction(s).Evaluate(context);
				return ret;
			}

			I = BuscarExterior('/', str);
			if (I.Any())
			{
				if (I.Length > 2)
					throw new Exception("Invalid format '/' in " + str);
				// Es Div
				string[] Ls = RompeEnÍndices(str, I);
				return new SymbolicFunction(Ls[0]).Evaluate(context) / new SymbolicFunction(Ls[1]).Evaluate(context);
			}

			I = BuscarExterior('<', str);
			if (I.Any())
			{
				if (I.Length != 1)
					throw new Exception("Invalid format '<' in " + str);
				// Es Div
				string[] Ls = RompeEnÍndices(str, I);
				var fnLeft = new SymbolicFunction(Ls[0]).Evaluate(context);
				var fnRight = new SymbolicFunction(Ls[1]).Evaluate(context);
				return fnLeft < fnRight ? 1 : 0;
			}

			I = BuscarExterior('>', str);
			if (I.Any())
			{
				if (I.Length != 1)
					throw new Exception("Invalid format '>' in " + str);
				// Es Div
				string[] Ls = RompeEnÍndices(str, I);
				var fnLeft = new SymbolicFunction(Ls[0]).Evaluate(context);
				var fnRight = new SymbolicFunction(Ls[1]).Evaluate(context);
				return fnLeft > fnRight ? 1 : 0;
			}

			//Es func?
			var P = ObtenerFuncParams(str);
			if (P.Formato)
			{
				//Es func!
				if (context.GetFunc(P.NomFunc) is IParametrizedSymbolicFunction<float> func)
				{
					//Y existe
					var fl = new float[P.Params.Length];
					for (int i = 0; i < P.Params.Length; i++)
						fl[i] = new SymbolicFunction(P.Params[i]).Evaluate(context);
					return func.Evaluate(context, fl);
				}

				throw new Exception(string.Format("Parametrized formula {0} not found in context.", P.NomFunc));
			}

			//Si no es otra cosa, entonces es variable
			return context.GetVar(str);
		}

		/// <summary>
		/// Descompone una cadena en varias subcadenas, haciendo la descomposición índices dados.
		/// </summary>
		/// <param name="s">Cadena a romper.</param>
		/// <param name="inds">Lista de índices donde se hará la ruptura.</param>
		/// <returns>Regresa un arreglo de subcadenas de la original.</returns>
		string[] RompeEnÍndices(string s, IList<int> inds)
		{
			var ret = new List<string>();
			if (inds.Any())
			{
				int i = inds[0];
				if (i < 1)
					throw new FormatException("En rompeíndices, I debe ser sucesion creciente, no arbitraria.");
				ret.Add(s.Substring(0, i));
				string Resto = s.Substring(i + 1);
				List<int> J = inds.ToList();
				J.RemoveAt(0);
				for (int n = 0; n < J.Count; n++)
				{
					J[n] -= i + 1;
				}
				foreach (string x in RompeEnÍndices(Resto, J.ToArray()))
				{
					ret.Add(x);
				}
			}
			else
			{
				ret.Add(s);
			}
			return ret.ToArray();
		}

		/// Evaluates a <c>string</c> well-formatted expression.
		/// <param name="expression">Expression.</param>
		public static float Evaluate(string expression) => new SymbolicFunction(expression).Evaluate();

		/// Evaluates a <c>string</c> well-formatted expression.
		/// <param name="expression">Expression.</param>
		/// <param name="context">Evaluation context.</param>
		public static float Evaluate(string expression, IEvalContext<float> context) => new SymbolicFunction(expression).Evaluate(context);

		/// <summary>
		/// Busca caracteres dentro de una string pero que estén en el ()-nivel 0, ie exterior.
		/// </summary>
		/// <param name="c">Caracter a buscar.</param>
		/// <param name="s">Cadena dónde buscar.</param>
		/// <returns>Devuelve un arreglo de ints con las posiciones dónde encontró los caracteres.</returns>
		static int[] BuscarExterior(char c, string s)
		{
			int nivel = 0;
			int ind = 0;
			var ret = new List<int>();
			foreach (char d in s)
			{
				if (d == Convert.ToChar("("))
					nivel++;
				else if (d == Convert.ToChar(")"))
					nivel--;
				else if ((nivel) == 0 && (c == d))
					ret.Add(ind);
				ind++;
			}
			return ret.ToArray();
		}

		FuncParams ObtenerFuncParams(string s)
		{
			int i;
			var ret = new FuncParams();

			if (s.Contains("("))
			{
				i = s.IndexOf('(');
				ret.Formato = s.Last() == ')';
				if (ret.Formato)
				{
					ret.NomFunc = s.Substring(0, i).Trim();
					string Par = s.Substring(i + 1, s.Length - (i + 2));
					ret.Params = RompeEnÍndices(Par, BuscarExterior(',', Par));
				}
				else
				{
					ret.Formato = false;
					return ret;
				}
			}
			else
			{
				ret.Formato = false;
			}
			return ret;
		}

		/// <summary>
		/// Devuelve true sólo si la string representa un número y puede ser convertido sin errores.
		/// </summary>
		/// <param name="s">string que podría representar un número.</param>
		/// <returns></returns>
		static bool EsNúmero(string s)
		{
			s = s.Trim();
			double tmp;
			return double.TryParse(s, out tmp);
		}
		struct FuncParams
		{
			/// <summary>
			/// Es true sólo si tiene el formato correcto para ser función.
			/// </summary>
			public bool Formato;
			/// <summary>
			/// Nombre de la función
			/// </summary>
			public string NomFunc;
			/// <summary>
			/// Lista de parámetros de la función.
			/// </summary>
			public string[] Params;
		}
	}
}
