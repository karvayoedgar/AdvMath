namespace CE.Math.Symbolic
{
 /// Context for evaluations
 public class FunctionEvaluationContext : IEvalContext<float>
 {
     /// Gets the itertive function collection.
     public ContextFunctionCollection Funcs;

     /// Gets the variables collection.
     public ContextVariableCollection Vars;

     /// Initializes a new instance of the <see cref="FunctionEvaluationContext"/> class.
     public FunctionEvaluationContext()
     {
         Funcs = new ContextFunctionCollection();
         Vars = new ContextVariableCollection();
     }

     /// Evaluates a <see cref="ISymbolicFunction{T}"/>
     public float Evaluate(ISymbolicFunction<float> fml) => fml.Evaluate(this);

     ISymbolicFunction<float> IEvalContext<float>.GetFunc(string str) => Funcs[str];

     float IEvalContext<float>.GetVar(string str) => Vars[str];
 }
}