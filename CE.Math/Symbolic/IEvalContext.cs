
namespace CE.Math.Symbolic
{
	/// Function evaluation context.
	public interface IEvalContext<T>
	{
		/// Gets the value of a variable.
		T GetVar (string str);

		/// Gets a function.
		ISymbolicFunction<T> GetFunc (string str);
	}
}