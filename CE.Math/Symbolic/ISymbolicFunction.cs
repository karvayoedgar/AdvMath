
namespace CE.Math.Symbolic
{
	/// <summary>
	/// A fórmula
	/// </summary>
	public interface ISymbolicFunction<T>
	{
		/// <summary>
		/// Evaluates
		/// </summary>
		T Evaluate (IEvalContext<T> context);
	}

	/// <summary>
	/// Parametrized fórmula.
	/// </summary>
	public interface IParametrizedSymbolicFunction<T> : ISymbolicFunction<T>
	{
		/// <summary>
		/// Evaluates
		/// </summary>
		T Evaluate (IEvalContext<T> context, T [] values);
	}
}