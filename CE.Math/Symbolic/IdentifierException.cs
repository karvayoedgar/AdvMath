﻿using System;

namespace CE.Math.Symbolic
{
	/// <summary>
	/// Exception concerning the access of an invalid object of some kind.
	/// </summary>
	[Serializable]
	public class IdentifierException : Exception
	{
		/// Missing identifier.
		public string Identifier { get; }
		/// Type of identifier.
		public IdentifierType Type { get; }
		/// Initializes a new instance of the <see cref="IdentifierException"/> class
		public IdentifierException(string identifier, IdentifierType type) : this(identifier, type, null)
		{
		}

		/// Initializes a new instance of the <see cref="IdentifierException"/> class
		public IdentifierException(string identifier, IdentifierType type, Exception inner) : base(MessageBuilder(identifier), inner)
		{
			Identifier = identifier;
			Type = type;
		}

		/// Initializes a new instance of the <see cref="T:IdentifierException"/> class
		protected IdentifierException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
		{
		}

		static string MessageBuilder(string id) => $"Identifier {id} unknown.";

		/// <summary>
		/// Type of identifier.
		/// </summary>
		public enum IdentifierType
		{
			/// Object is a variable.
			Variable,
			/// Object is a function.
			Function
		}
	}
}