using System;

namespace CE.Math.Symbolic
{
	/// A <see cref="IParametrizedSymbolicFunction{T}"/> that invokes a <see cref="System.Func{T}"/>
	public class SymbolicFunc<T> : IParametrizedSymbolicFunction<T>
	{
		Func<IEvalContext<T>, T [], T> _func { get; }
		/// Initializes a new instance of the <see cref="SymbolicFunc{T}"/> class.
		public SymbolicFunc (Func<IEvalContext<T>, T [], T> @delegate)
		{
			_func = @delegate;
		}

		/// Evaluates this function with a specified context and arguments.
		public T Evaluate (IEvalContext<T> context, T [] arguments) => _func.Invoke (context, arguments);

		T ISymbolicFunction<T>.Evaluate (IEvalContext<T> context) => Evaluate (context, new T [0]);
	}
}