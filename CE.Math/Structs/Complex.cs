﻿using System;

namespace CE.Math.Structs
{
	/// <summary>
	/// A (double) complex number.
	/// </summary>
	public struct Complex : IEquatable<double>, IEquatable<Complex>
	{
		/// Gets the real part.
		public double Real { get; }

		/// Gets the imaginary part.
		public double Imag { get; }

		/// Gets the norm.
		public double Norm => System.Math.Sqrt (Real * Real + Imag * Imag);

		/// Gets the square of the norm.
		public double SquaredNorm => Real * Real + Imag * Imag;

		/// Gets the argument.
		public double Argument => System.Math.Atan2 (Imag, Real);

		/// Initialize a complex number.
		public Complex (double rreal, double rimag)
		{
			Real = rreal;
			Imag = rimag;
		}

		/// Gets the complex number given its polar coordinates.
		/// <param name="norm">The norm.</param>
		/// <param name="arg">The argument.</param>
		public static Complex FromPolar (double norm, double arg)
		{
			double _real = norm * System.Math.Cos (arg);
			double _imag = norm * System.Math.Sin (arg);
			return new Complex (_real, _imag);
		}


		/// <summary>
		/// Returns a <see cref="String"/> that represents the current <see cref="Complex"/>.
		/// </summary>
		public override string ToString ()
		{
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
			if (Real == 0)
			{
				if (Imag == 0)
					return "0";
				if (Imag == 1)
					return "i";
				return Imag + "i";
			}
			{
				return Imag == 0 ? Real.ToString () : string.Format (
					"{0}+{1}i",
					Real,
					Imag);
			}
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
		}

		/// <summary>
		/// Determines whether the specified <see cref="object"/> is equal to the current <see cref="Complex"/>.
		/// </summary>
		public override bool Equals (object obj)
		{
			return obj is Complex objCmp && objCmp == this;
		}

		/// <summary>
		/// Serves as a hash function for a <see cref="T:AdvMath.Estructuras.Complex"/> object.
		/// </summary>
		public override int GetHashCode ()
		{
			return Real.GetHashCode () ^ Imag.GetHashCode ();
		}

		bool IEquatable<double>.Equals (double other)
		{
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
			return (Imag == 0 && Real == other);
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
		}

		bool IEquatable<Complex>.Equals (Complex other)
		{
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
			return (Imag == other.Imag && Real == other.Real);
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
		}

		/// <summary/>
		public static Complex operator + (Complex left, Complex right) => new Complex (left.Real + right.Real, left.Imag + right.Imag);

		/// <summary/>
		public static Complex operator * (Complex left, Complex right)
		{
			double Real = left.Real * right.Real - left.Imag * right.Imag;
			double Imag = left.Real * right.Imag + left.Imag * right.Real;
			return new Complex (Real, Imag);
		}

		/// <summary/>
		public static Complex operator ^ (Complex left, double right)
		{
			double _norm = System.Math.Pow (left.Norm, right);
			double _arg = left.Argument * right;
			return FromPolar (_norm, _arg);
		}

		/// <summary/>
		public static bool operator == (Complex left, Complex right)
		{
#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
			return left.Real == right.Real && left.Imag == right.Imag;
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator
		}

		/// <summary/>
		public static bool operator != (Complex left, Complex right)
		{
			return !(left == right);
		}

		/// <summary/>
		public static explicit operator Complex (double value) => new Complex (value, 0);
	}
}