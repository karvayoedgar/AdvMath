using System;
using System.Collections.Generic;
using System.Linq;

namespace CE.Math.Structs
{
 /// Provides methods about divisibility of integers.
 public static class Numbers
 {
     /// Gets the max common divisor.
     public static long LCD(params long[] numbers) => numbers.Aggregate(LCD);

     /// Gets the max common divisor.
     public static long LCD(IEnumerable<long> numbers) => numbers.Aggregate(LCD);

     /// Gets the max common divisor.
     public static long LCD(long a, long b)
     {
         if (a == 0 || b == 0)
             throw new DivideByZeroException();
         if (a < 0 || b < 0)
             throw new InvalidOperationException("Values must be positive.");

         var max = System.Math.Max(a, b);
         var min = a + b - max;

         return LCD_Unchecked(max, min);
     }

     static long LCD_Unchecked(long max, long min)
     {
         var res = max % min;
         // Euclid's algorithm.
         return res == 0 ? min : LCD(min, res);
     }

     /// Factorizes a positive number.
     /// <returns>Returns an enumeration of the prime decomposition.</returns>
     public static IEnumerable<long> Factor(long num)
     {
         if (num <= 0) throw new InvalidOperationException("Value must be positive.");

         var currDiv = 2;
         while (currDiv * currDiv <= num)
         {
             if (num % currDiv == 0)
             {
                 num /= currDiv;
                 yield return currDiv;
             }
             else
             {
                 currDiv++;
             }
         }
         if (num > 1)
             yield return num;
     }

     //TEST
     /// Determines whether a specified number is a prime number.
     public static bool IsPrime(long num)
     {
         if (num == 0) throw new InvalidOperationException("Value cannot be zero.");
         if (num <= 1) return false; // Nor negative nor 1 are primes.
         var currDiv = 2;
         while (currDiv * currDiv <= num)
         {
             if (num % currDiv == 0)
                 return false;
             currDiv++;
         }
         return num == 1;
     }

     /// Factorizes a positive number.
     /// <returns>Returns a dictionary associating every prime to its occurence times.</returns>
     // TODO: return CE.Collection.Bag instead? Or a specialized class?
     public static Dictionary<long, int> PrimeDecomposition(long num)
     {
         var dict = new Dictionary<long, int>();
         foreach (var x in Factor(num))
         {
             if (!dict.ContainsKey(x))
                 dict.Add(x, 0);
             dict[x]++;
         }
         return dict;
     }
 }
}