﻿using System;

namespace CE.Math.Structs
{
	/// A rational number.
	public struct Rational : IComparable<Rational>, IEquatable<Rational>, IComparable<double>, IEquatable<double>
	{
		/// Gets the numerador.
		public long Num { get; }
		/// Gets the denominador.
		public long Denom => _denomDefase + 1;
		/// Gets the positive infinite value.
		public static Rational PositiveInfinity { get; }
		/// Gets the negative infinite value.
		public static Rational NegativeInfinity { get; }
		/// Gets the <c>NaN</c> constant.
		public static Rational NaN { get; }

		/// Initializes a new instance of the <see cref="Rational"/> struct.
		public Rational (long num, long den)
		{
			if (den != 0)
			{
				var div = num == 0 ? 1 : Numbers.LCD (System.Math.Abs (num), System.Math.Abs (den));
				var divSgn = System.Math.Sign (den);
				Num = num / div;
				_denomDefase = (den / div) - 1;
			}
			else
			{
				Num = System.Math.Sign (num);
				_denomDefase = -1;
			}
		}

		/// Returns a <see cref="String"/> that represents the current <see cref="Rational"/>.
		public override string ToString ()
		{
			switch (Denom)
			{
			case 1:
				return Num.ToString ();
			case 0:
				if (Num == 0)
					return "NAN";
				if (Num > 0)
					return "INFINITY";
				return "-INFINITY";
			default:
				if (Num == 0)
					return "0";
				return string.Format ("{0} / {1}", Num, Denom);
			}
		}

		/// Determines whether the specified <see cref="object"/> is equal to the current <see cref="Rational"/>.
		public override bool Equals (object obj) => obj is Rational rational && rational == this;

		/// Serves as a hash function for a <see cref="Rational"/> object.
		public override int GetHashCode () => Num.GetHashCode () ^ _denomDefase.GetHashCode ();

		readonly long _denomDefase;

		/// Creates a <see cref="Rational"/>.
		public static Rational Create (int num, int den = 1) => new Rational (num, den);

		/// Aproximates a double with a <see cref="Rational"/>.
		/// <param name="num">Double Number.</param>
		/// <param name="maxDenom">Max denominator</param>
		public static Rational FromDouble (double num, int maxDenom)
		{
			if (double.IsNaN (num))
				return NaN;
			if (double.IsPositiveInfinity (num))
				return PositiveInfinity;
			if (double.IsNegativeInfinity (num))
				return NegativeInfinity;

			int currNum = 0;
			int currDen = 1;
			Rational ret = 0;

			double unsignedNum = System.Math.Abs (num);
			while (currDen < maxDenom)
			{
				double currN = currNum / (double)currDen;
				if (System.Math.Abs (ret - num) > System.Math.Abs (currN - num))
					ret = new Rational (currNum, currDen);
				if (currN < unsignedNum)
					currNum++;
				else if (currN > unsignedNum)
					currDen++;
				else
					return new Rational (currNum * System.Math.Sign (num), currDen);
			}

			return ret;
		}

		/// <summary/>
		public static implicit operator double (Rational r) => r.Num / (double)r.Denom;

		/// <summary/>
		public static implicit operator Rational (int r) => new Rational (r, 1);
		/// <summary/>
		public static implicit operator Rational (long r) => new Rational (r, 1);
		/// <summary/>
		public static implicit operator Rational (byte r) => new Rational (r, 1);

		bool IEquatable<Rational>.Equals (Rational other) => this == other;

#pragma warning disable RECS0018 // Comparison of floating point numbers with equality operator
		bool IEquatable<double>.Equals (double other) => this == other;
#pragma warning restore RECS0018 // Comparison of floating point numbers with equality operator

		int IComparable<Rational>.CompareTo (Rational other) => this < other ? -1 : (this == other ? 0 : 1);

		int IComparable<double>.CompareTo (double other)
		{
			double r = Num / Denom;
			return r.CompareTo (other);
		}

		static Rational ()
		{
			PositiveInfinity = new Rational (1, 0);
			NegativeInfinity = new Rational (-1, 0);
			NaN = new Rational (0, 0);
		}

		/// <c>NaN</c> is 0/0.
		public static bool IsNaN (Rational number) => number.Num == 0 && number.Denom == 0;

		/// Gets a value indicating whether this <see cref="Rational"/> represents a positive infinite value.
		public static bool IsPositiveInfinity (Rational number) => number.Denom == 0 && number.Num > 0;

		/// Gets a value indicating whether this <see cref="Rational"/> represents a negative infinite value.
		public static bool IsNegativeInfinity (Rational number) => number.Denom == 0 && number.Num < 0;

		/// Gets a value indicating whether this <see cref="Rational"/> represents a infinite value.
		public static bool IsInfinity (Rational number) => number.Denom == 0 && number.Num != 0;

		/// Determina si es un número racional
		public static bool IsNumber (Rational number) => number.Denom != 0;

		/// <summary/>
		public static Rational operator + (Rational left, Rational right)
		{
			var nnum = left.Num * right.Denom + right.Num * left.Denom;
			var nden = left.Denom * right.Denom;
			return new Rational (nnum, nden);
		}

		/// <summary/>
		public static Rational operator * (Rational left, Rational right)
		{
			return new Rational (
				left.Num * right.Num,
				left.Denom * right.Denom);
		}

		/// <summary/>
		public static bool operator < (Rational left, Rational right) => left.Num * right.Denom < right.Num * left.Denom;

		/// <summary/>
		public static bool operator <= (Rational left, Rational right) => left.Num * right.Denom <= right.Num * left.Denom;

		/// <summary/>
		public static bool operator > (Rational left, Rational right) => left.Num * right.Denom > right.Num * left.Denom;

		/// <summary/>
		public static bool operator >= (Rational left, Rational right) => left.Num * right.Denom >= right.Num * left.Denom;

		/// <summary/>
		public static bool operator == (Rational left, Rational right) => left.Num == right.Num && left.Denom == right.Denom;

		/// <summary/>
		public static bool operator != (Rational left, Rational right) => !(left == right);

		// TODO: Do not have this func here
		/// Raises a <see cref="Rational"/> to a power.
		/// <param name="base">Base.</param>
		/// <param name="pow">Power.</param>
		public static Rational Pow (Rational @base, int pow)
		{
			// TODO: implement when pow < 0
			if (IsNaN (@base))
				return @base;
			if (IsInfinity (@base))
				return pow == 0 ? NaN : @base;
			if (pow == 0)
				return @base == 0 ? NaN : 1;
			var r = Pow (@base, pow / 2);
			if (pow % 2 == 0)
				return r * r;
			return r * r * @base;
		}
	}
}